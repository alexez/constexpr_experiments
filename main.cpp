#include "int_to_string.hpp"

#include <cstdio>

int main() {
    std::printf("%s", int_to_string::get_string<1234>().data());
    constexpr auto digits = int_to_string::digits_count(1234);
    return digits;
}
