#pragma once

#include <array>
#include <limits>

namespace int_to_string {

    template<std::size_t size>
    using string_t = std::array<char, size>;

    template<unsigned power, unsigned current = 0u>
    constexpr auto power_of_10() {
        if constexpr (current == power) {
            return 1u;
        } else {
            return power_of_10<power, current + 1u>() * 10u;
        }
    }

    template<unsigned current_digit, unsigned max_digits_amount>
    constexpr auto _digits_count([[maybe_unused]] unsigned number) {
        if constexpr (current_digit <= max_digits_amount) {
            bool is_digit_presented = ((number / power_of_10<current_digit>()) % 10u != 0u);
            auto significant_digits_result = _digits_count<current_digit + 1u, max_digits_amount>(number);
            return std::max(significant_digits_result, is_digit_presented ? current_digit : 0u);
        }
        return 0u;
    }

    constexpr auto digits_count(unsigned number) {
        constexpr auto max_digits_amount = std::numeric_limits<decltype(number)>::digits10 + 1u;
        return _digits_count<0u, max_digits_amount>(number) + 1u;
    }

    template<unsigned current_digit = 0u, auto size>
    constexpr void fill_with_number([[maybe_unused]] string_t<size> &string, [[maybe_unused]] unsigned number) {
        if constexpr (current_digit < size) {
            string[size - current_digit - 1u] = '0' + (number / power_of_10<current_digit>()) % 10u;
            fill_with_number<current_digit + 1u>(string, number);
        }
    }


    template<unsigned number>
    constexpr auto get_string() {
        constexpr auto digits = digits_count(number);

        string_t<digits> result{};
        fill_with_number(result, number);
        return result;
    }

}
